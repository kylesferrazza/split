import type { RollupOptions } from 'rollup';

import svelte from 'rollup-plugin-svelte';
import resolve from '@rollup/plugin-node-resolve';
import css from "rollup-plugin-import-css";

import autoPreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';

const config: RollupOptions = {
  input: 'index.js',
  output: {
    format: 'iife',
    file: 'public/index.js',
    sourcemap: false,
  },
  plugins: [
    css(),
    svelte({
      emitCss: false,
      preprocess: autoPreprocess(),
    }),
    resolve({
      browser: true,
      dedupe: ['svelte'],
    }),
    typescript({
      rootDir: './src',
      sourceMap: false,
    }),
  ],
};

export default config;
