import type { RollupOptions } from 'rollup';

import cfg from './rollup.config';

import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload';

const config: RollupOptions = {
  ...cfg,
  plugins: [
    ...(cfg.plugins || []),
    serve({
      contentBase: 'public',
    }),
    livereload(),
  ]
};

export default config;
