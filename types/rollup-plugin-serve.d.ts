declare module 'rollup-plugin-serve' {
  import { Plugin } from 'rollup';
  export default function serve({
    contentBase: string,
  }): Plugin;
};
