export function individualPrice(origPrice: number, numAssignments: number, myIndex: number) {
  const origCents = Math.floor(origPrice*100);
  const quotient = origCents / numAssignments;
  const whole = Math.floor(quotient);
  const remainder = quotient - whole;
  const numPeopleWithExtra = Math.round(remainder*numAssignments);
  const meExtra = myIndex < numPeopleWithExtra;

  let meCents = whole;
  if (meExtra) {
    meCents += 1;
  }

  return meCents/100;
}
