
import { individualPrice } from './numUtils';

interface Example {
  orig: number;
  people: number[];
}

const examples: Example[] = [
  {
    orig: 3.32,
    people: [1.66, 1.66],
  },
  {
    orig: 3.33,
    people: [1.67, 1.66],
  },
  {
    orig: 3.34,
    people: [1.67, 1.67],
  },
  {
    orig: 3.33,
    people: [1.11, 1.11, 1.11],
  },
  {
    orig: 3.34,
    people: [1.12, 1.11, 1.11],
  },
  {
    orig: 3.35,
    people: [1.12, 1.12, 1.11],
  },
  {
    orig: 3.36,
    people: [1.12, 1.12, 1.12],
  },
  {
    orig: 3.37,
    people: [1.13, 1.12, 1.12],
  },
];

examples.forEach((example) => {
  example.people.forEach((personExpected, personIndex) => {
    const res = individualPrice(example.orig, example.people.length, personIndex);
    const testMsg = `price of $${example.orig} with ${example.people.length} people, person ${personIndex} should be $${personExpected}`;
    test(testMsg, () => {
      expect(res).toBe(personExpected);
    });
  });
});

